<?php

namespace shop\readModels\User;

use shop\repositories\UserRepository;
use shop\entities\User\User;

class UserReadRepository
{
    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function findActiveById($id): ?User
    {
        return $this->users->findActiveById($id) ?? null;
    }

    public function find($id): ?User
    {
        return $this->users->findOne($id);
    }
}
